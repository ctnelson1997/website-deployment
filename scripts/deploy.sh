DEPLOY_PATH="/coletnelson/website/docker"
cd $DEPLOY_PATH

export NG_CLI_ANALYTICS=ci

VER_NODE=$1
VER_ANGULAR=$2

PORT=4200

echo "--------------------------------------------"
echo "BEGINNING PRODUCTION DEPLOYMENT OF WEBSITE"
echo "--------------------------------------------"

echo "Entering source directory..."
cd temp
echo "Done!"

echo "Moving Dockerfile into source directory..."
cp ../includes/Dockerfile Dockerfile
echo "Done!"

echo "Building Docker image of application using Angular v$VER_ANGULAR, Node v$VER_NODE on :$PORT..."
docker build \
-t website:prod \
--build-arg PORT=$PORT \
--build-arg VER_NODE=$VER_NODE \
--build-arg VER_ANGULAR=$VER_ANGULAR \
--no-cache .
echo "Done! Image build complete."

echo "Preparing for deployment..."
echo "Searching for existing container of production website..."
TMP_OLD_CONTAINER_ID=$(docker ps -a | grep website | awk '{ print $1 }')
echo "Found ID: '$TMP_OLD_CONTAINER_ID'"
echo "Done!"

echo "Stopping container of production website..."
docker stop $TMP_OLD_CONTAINER_ID
echo "Done!"

echo "Deleting container of production website..."
docker rm -f $TMP_OLD_CONTAINER_ID
echo "Done!"

echo "Booting new production website container on :$PORT"
docker run --restart=always -d -p $PORT:$PORT --name website website:prod
echo "Done!"

echo "Removing old source code..."
cd ..
rm -rf temp
echo "Done!"

echo "Process complete. Website container deployed to production! Angular may take a minute to boot."
